/**
 * Execution entry point
 *
 * @author Zaid Hoona
 */

var args = process.argv.slice(2);
var sum = 0;

for (var i = 0; i < args.length; i++) {
    args[i] = Number(args[i]);
}

for (var i = 0; i < args.length; i++) {
    sum = sum + args[i];
}
console.log("The sum is " + sum);

// With pipe functions
//
// var args = process.argv.slice(2);
// args = args.map(function (num) { return Number(num) });
//
// var sum = args.reduce(function (prev, curr) { return prev + curr });
// console.log("The sum is " + sum);

// ES6 equivalent
//
// let args = process.argv.slice(2).map(num => Number(num));
// let sum = args.reduce((prev, curr) => prev + curr);
// console.log(`The sum is ${sum}`);